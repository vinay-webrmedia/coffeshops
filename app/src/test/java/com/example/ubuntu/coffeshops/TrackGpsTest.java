package com.example.ubuntu.coffeshops;

import android.content.Context;
import android.location.Location;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import android.location.LocationManager;
import android.test.mock.MockContext;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * Created by ubuntu on 19/4/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TrackGpsTest {
    private static final double LATITUDE = -38.010403;
    private static final double LONGITUDE = -57.558408;
    private TrackGps gps;
    private MapsActivity mapsActivity;
    private Location loc;
    @Mock
    Context mMockContext;
    @Before
    public void setUp() throws Exception {
        mapsActivity = mock(MapsActivity.class);
        mMockContext = new MockContext();
        gps = new TrackGps(mMockContext, mapsActivity);
        loc = mock(Location.class);
    }

    @After
    public void tearDown() throws Exception {
        mapsActivity = null;
        mMockContext = null;
        gps = null;
        loc = null;
    }
    @Test
    public void getLongitude() throws Exception {
        gps.loc = loc;
        when(gps.loc.getLongitude()).thenReturn(LONGITUDE);
        assertEquals(loc.getLongitude(), gps.getLongitude(), 0);
    }

    @Test
    public void getLatitude() throws Exception {
        gps.loc = loc;
        when(gps.loc.getLatitude()).thenReturn(LATITUDE);
        assertEquals(loc.getLatitude(), gps.getLatitude(), 0);
    }

    @Test
    public void canGetLocation() throws Exception {
        gps.canGetLocation = true;
        assertEquals(true, gps.canGetLocation());
    }

    @Test
    public void negativeCanGetLocation() throws Exception {
        gps.canGetLocation = false;
        assertEquals(false, gps.canGetLocation());
    }

    @Test
    public void negativeGetLongitude() throws Exception {
        gps.loc = null;
        assertEquals(0.0, gps.getLongitude(), 0);
    }

    @Test
    public void negativeGetLatitude() throws Exception {
        gps.loc = null;
        assertEquals(0.0, gps.getLatitude(), 0);
    }
}