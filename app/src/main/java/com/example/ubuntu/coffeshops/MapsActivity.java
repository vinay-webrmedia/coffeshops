package com.example.ubuntu.coffeshops;

import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private UiSettings uiSettings;
    private double latitude, longitude, last_latitude, last_longitude;
    private TrackGps gps;
    private HttpServiceHandler httpServiceHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }


    protected void onStop() {
        mGoogleApiClient.disconnect();
        gps.stopUsingGPS();
        super.onStop();
    }

    protected void onDestroy() {
        gps.stopUsingGPS();
        super.onDestroy();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Configure Map
        mMap = googleMap;
        uiSettings = mMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setAllGesturesEnabled(true);
        mMap.setMinZoomPreference(15);
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(MapsActivity.this));
    }

    public void onConnected(Bundle connectionHint) {
        // Set location once map is connected
        initMap();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    // User Permission Request handler
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, Set location
                    getPlaces();

                } else {
                    // permission denied
                    Log.e("Permission", "Location Permission Denied");
                }
                return;
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    // Method gets user location and adds places to the map
    public void initMap() {
        // Check if we have permission to access location
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e("Permission Error", "No permission for location");
            Log.e("Permission", "Requesting Location permission");
            // Request for permission if permission is not granted
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            return;
        }
        mMap.setMyLocationEnabled(true);
        getPlaces();
    }

    // Add marker on the map
    private void addMarkerToMap(double longitude, double latitude, String title) {
        LatLng location = new LatLng(latitude, longitude);
        Marker marker = mMap.addMarker(new MarkerOptions().position(location).flat(true));
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher);
        marker.setIcon(icon);
        marker.setTitle(title);
        if (last_longitude != this.longitude && last_latitude != this.latitude) {
            location = new LatLng(this.latitude, this.longitude);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        }
    }

    // Parse the places data returned by Google Places webservice and add places to map
    private void parseData(String response) {
        if (response != null) {
            JSONObject obj = null;
            try {
                obj = new JSONObject(response);
                JSONArray results = new JSONArray(obj.getString("results"));
                for (int i = 0; i < results.length(); i++) {
                    JSONObject location = results.getJSONObject(i).getJSONObject("geometry").getJSONObject("location");
                    StringBuilder title = new StringBuilder(results.getJSONObject(i).getString("name"));
                    // "#$" is used as field seperator
                    title.append("#$");
                    title.append(results.getJSONObject(i).getString("vicinity"));
                    if (!results.getJSONObject(i).isNull("opening_hours")) {
                        title.append("#$");
                        title.append(results.getJSONObject(i).getJSONObject("opening_hours").getBoolean("open_now") ? "Open" : "Closed");
                    }
                    addMarkerToMap(location.getDouble("lng"), location.getDouble("lat"), title.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void getPlaces() {
        gps = new TrackGps(MapsActivity.this, this);
        gps.setLocation();

        if(gps.canGetLocation()){
            last_latitude = latitude;
            last_longitude = longitude;
            longitude = gps.getLongitude();
            latitude = gps .getLatitude();
        }
        else
        {
            gps.showSettingsAlert();
        }

        // Update map only if coordinates are changed
        if (last_longitude != longitude && last_latitude != latitude) {
            // Removes all markers, overlays, and polylines from the map.
            mMap.clear();
            StringBuilder url = new StringBuilder(getString(R.string.places_api_url));
            url.append("?location=" + latitude + "," + longitude);
            url.append("&radius=1000");
            url.append("&types=cafe");
            url.append("&key=" +  getString(R.string.places_api_key));
            String response = makeHttpReuqest(url.toString());
            parseData(response);
        }
    }

    public String makeHttpReuqest(String url) {
        String response = null;
        httpServiceHandler = new HttpServiceHandler();
        httpServiceHandler.execute(url.toString());
        try {
            response = httpServiceHandler.get();
            return response;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return response;
    }
}
