package com.example.ubuntu.coffeshops;


import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ubuntu on 18/4/17.
 */

public class HttpServiceHandler extends AsyncTask<String, Void, String> {
    private static final String TAG = HttpServiceHandler.class.getSimpleName();

    public HttpServiceHandler() {

    }

    @Override
    protected String doInBackground(String... urls) {
        try {
            String api_url = urls[0];
            URL url = new URL(api_url);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                return stringBuilder.toString();
            }
            finally{
                urlConnection.disconnect();
            }
        }
        catch(Exception e) {
            Log.e("ERROR", e.getMessage());
            return null;
        }
    }
}
