package com.example.ubuntu.coffeshops;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by ubuntu on 18/4/17.
 */

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private final View myContentsView;

    CustomInfoWindowAdapter(Context mContext){
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        myContentsView = inflater.inflate(R.layout.custom_info, null);
    }

    @Override
    public View getInfoContents(Marker marker) {
        TextView tvTitle = ((TextView)myContentsView.findViewById(R.id.name));
        String title = marker.getTitle();
        String[] parts = title.split("#\\$");
        tvTitle.setText(parts[0]);
        TextView tvAddress= ((TextView)myContentsView.findViewById(R.id.address));
        tvAddress.setText(parts[1]);
        TextView tvOpen = ((TextView)myContentsView.findViewById(R.id.open));
        tvOpen.setText("");
        if (parts.length > 2) {
            if (parts[2].equals("Open")) {
                tvOpen.setTextColor(Color.parseColor("#108724"));
            }
            else {
                tvOpen.setTextColor(Color.RED);
            }
            tvOpen.setText(parts[2]);
        }

        return myContentsView;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        // TODO Auto-generated method stub
        return null;
    }
}
